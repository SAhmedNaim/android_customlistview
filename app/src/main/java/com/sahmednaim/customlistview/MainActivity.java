package com.sahmednaim.customlistview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        listView = (ListView) findViewById(R.id.list_item);

        String[] country = {"Australia", "Bangladesh", "Brazil", "Canada", "China", "India", "Japan", "Korea", "Nepal", "Singapour", "United Kingdom", "USA"};
        int[] image = {R.drawable.australia, R.drawable.bangladesh, R.drawable.brazil, R.drawable.canada, R.drawable.china, R.drawable.india, R.drawable.japan, R.drawable.korea, R.drawable.nepal, R.drawable.singapore, R.drawable.uk, R.drawable.usa};

        MyCustomAdapter myCustomAdapter = new MyCustomAdapter(this, country, image);

        listView.setAdapter(myCustomAdapter);

    }
}
