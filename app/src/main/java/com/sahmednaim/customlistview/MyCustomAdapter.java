package com.sahmednaim.customlistview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by DIU on 31-Jul-18.
 */

public class MyCustomAdapter extends BaseAdapter
{
    Context context;
    String[] country;
    int[] image;

    private static LayoutInflater layoutInflater = null;

    public MyCustomAdapter(MainActivity mainActivity, String[] nameOfCountry, int[] imageOfCountry)
    {
        this.context = mainActivity;
        this.country = nameOfCountry;
        this.image = imageOfCountry;

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount()
    {
        return this.country.length;
    }

    @Override
    public Object getItem(int i)
    {
        return i;
    }

    @Override
    public long getItemId(int i)
    {
        return i;
    }

    public class MyHolder
    {
        TextView textView;
        ImageView imageView;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup)
    {
        MyHolder myHolder = new MyHolder();

        View myView;

        myView = layoutInflater.inflate(R.layout.layout_custom_listview, null);

        myHolder.textView = (TextView) myView.findViewById(R.id.text_view);
        myHolder.imageView = (ImageView) myView.findViewById(R.id.image_view);

        myHolder.textView.setText(country[i]);
        myHolder.imageView.setImageResource(image[i]);

        myView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Toast.makeText(context, "Clicked On: " + country[i], Toast.LENGTH_SHORT).show();
            }
        });

        return myView;
    }
}
